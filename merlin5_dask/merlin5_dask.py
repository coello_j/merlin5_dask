import os
from os.path import join, expanduser
import time
import subprocess
import tempfile
from collections import namedtuple
from typing import List, Generator
from contextlib import contextmanager
from joblib import parallel_backend
from dask.distributed import Client


_SCHEDULER_FILE = join(expanduser("~"), ".scheduler_file.json")

Job = namedtuple("Job", ("job_id", ))


@contextmanager
def parallel_merlin5(n_workers: int,
                     wait_all: bool = True, bindir: str = None):
    """Wraps the Dask joblib parallel_backend context on the merlin5 context.

    This context manager acts the same as :func:`merlin5` but also activates a
    Dask joblib parallel_backend. Anything running using the joblib parallel
    API will be launched as tasks for the workers living in the Merlin5
    cluster. This context will wait indefinitely until at least one worker has
    joined the scheduler.

    This can be used to run scikit-learn optimization tasks in the cluster
    easily.

    Example:
        >>> from sklearn.datasets import load_digits
        >>> from sklearn.model_selection import GridSearchCV
        >>> from sklearn.ensemble import RandomForestClassifier
        >>> digits = load_digits()
        >>> param_space = [{'max_depth': range(6, 10)}]
        >>> model = RandomForestClassifier(n_estimators=100)
        >>> with parallel_merlin5(4, 4) as client:
        >>>     search = GridSearchCV(model, param_space, cv=5, verbose=10)
        >>>     search.fit(digits.data, digits.target)
        >>>     print(search.best_params_)

    Arguments:
        n_workers: Number of Dask workers to start.
        wait_all: If true, will wait for all wokers to spawn. Otherwise it will
            only wait for one worker to spawn.
        bindir: Directory containing the Dask executabes. Defaults to whatever
            is in the PATH.

    Yields:
        A Dask client to send task manually.
    """
    with merlin5(n_workers, bindir=bindir) as client:
        _wait_for_workers(client, n_workers, wait_all)
        with parallel_backend("dask"):
            yield client


@contextmanager
def merlin5(n_workers: int, bindir: str = None):
    """Starts n_workers Dask workers each in one Merlin5 node.

    This context manager will start n_workers Dask workers, each in one Merlin5
    node and a local Dask Scheduler on entry. It will kill the scheduler and
    cancel the worker jobs on exit.

    Example:
        >>> with merlin5(4, 8):
        >>>     # A local scheduler and 8 workers are started on 4 nodes.
        >>>     ...do parallel computations...
        >>> # The local scheduler and the jobs are canceled.

    Arguments:
        n_workers: Number of Dask workers to start.
        wait_all: If true, will wait for all wokers to spawn. Otherwise it will
            only wait for one worker to spawn.
        bindir: Directory containing the Dask executabes. Defaults to whatever
            is in the PATH.

    Yields:
        A Dask client to send task manually.
    """
    sch = _start_scheduler(bindir)
    jobs = _start_workers(n_workers)
    client = Client(scheduler_file=_SCHEDULER_FILE)
    try:
        yield client
    finally:
        _cancel_jobs(jobs)
        sch.kill()


def _start_scheduler(bindir: str = None):
    sch_exe = "dask-scheduler"
    if bindir is not None:
        sch_exe = join(bindir, sch_exe)
    command = [f"{sch_exe}", "--scheduler-file", f"{_SCHEDULER_FILE}"]
    return subprocess.Popen(command)


def _start_workers(n_workers: int, bindir: str = None)\
        -> List[Job]:
    worker_exe = "dask-worker"
    if bindir is not None:
        worker_exe = join(bindir, worker_exe)
    launch_worker = f"sbatch {worker_exe} --scheduler-file {_SCHEDULER_FILE}"
    file_content = "\n".join(["#!/bin/bash"] + [launch_worker] * n_workers)
    with _bash_script(file_content) as filepath:
        command = [filepath]
        proc = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        out, err = proc.communicate()
        out, err = out.decode(), err.decode()
        if proc.returncode != 0:
            raise ValueError(
                "Command exited with non-zero exit code.\n"
                "Exit code: {}\n"
                "Command:\n{}\n"
                "stdout:\n{}\n"
                "stderr:\n{}\n".format(proc.returncode,
                                       " ".join(command), out, err))
    return _workers_from_output(out, n_workers)


def _workers_from_output(submit_out: str, expect: int) -> List[Job]:
    jobs = [Job(int(line.split()[-1]))
            for line in submit_out.splitlines() if line]
    assert len(jobs) == expect,\
        ValueError(
            f"Unexpected output from start workers job.:\n{submit_out}"
        )
    return jobs


def _cancel_jobs(jobs: List[Job]):
    for job in jobs:
        subprocess.Popen(["scancel", str(job.job_id)])


def _wait_for_workers(client: Client, n_workers: int, wait_all: bool = True):
    waitfor = n_workers if wait_all else 1
    print(f"Waiting for {waitfor} workers to join...")
    while len(client.scheduler_info()['workers']) < waitfor:
        time.sleep(1.0)
    print(f"Done waiting for {waitfor} workers to join.")


@contextmanager
def _bash_script(contents: str) -> Generator[str, None, None]:
    try:
        fds, filepath = tempfile.mkstemp()
        os.chmod(filepath, 0o755)
        with open(filepath, "w") as filedata:
            filedata.write(contents)
        os.close(fds)
        yield filepath
    finally:
        os.remove(filepath)
