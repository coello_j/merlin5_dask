import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="merlin5_dask",
    version="0.0.1",
    author="Jaime Coello de Portugal",
    author_email="jaime.coello@psi.ch",
    description="Easy parallel jobs in PSI Merlin5",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.psi.ch/coello_j/merlin5_dask.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
